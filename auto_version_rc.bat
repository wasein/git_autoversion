@echo off

setlocal enabledelayedexpansion

rem not really necessary but can make troubleshooting less verbose
set FILEPATH=%~dp0
set REL_FILEPATH=!FILEPATH:*%cd%\=!

rem set all the corresponding environment variables
call %REL_FILEPATH%auto_version.bat

set GIT_VERSION_MEDIUM=%AVEXPORT_GIT_VERSION:-working=%
if "%GIT_VERSION_MEDIUM:~0,-9%"=="" (
	rem add the "-0" if this is a tag
	rem   this assumes the tag is less than 9 characters long
	rem   that should work up to xx.xx.xxx, and thus not fail anytime soon
	set GIT_VERSION_MEDIUM=%GIT_VERSION_MEDIUM%-0
) else (
	rem strip the hash off the end if it has one
	set GIT_VERSION_MEDIUM=%GIT_VERSION_MEDIUM:~0,-9%
)
set GIT_VERSION_MEDIUM=%GIT_VERSION_MEDIUM:.=,%
set GIT_VERSION_MEDIUM=%GIT_VERSION_MEDIUM:-=,%

set OUTPUT_FILE=autoversion-fixed.rc
echo FILEVERSION %GIT_VERSION_MEDIUM% > %OUTPUT_FILE%
echo PRODUCTVERSION %GIT_VERSION_MEDIUM% >> %OUTPUT_FILE%

set OUTPUT_FILE=autoversion.rc
echo VALUE "FileVersion", "%AVEXPORT_GIT_VERSION%" > %OUTPUT_FILE%
echo VALUE "ProductVersion", "%AVEXPORT_GIT_VERSION%" >> %OUTPUT_FILE%
