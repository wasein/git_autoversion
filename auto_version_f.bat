@echo off

setlocal enabledelayedexpansion

rem not really necessary but can make troubleshooting less verbose
set FILEPATH=%~dp0
set REL_FILEPATH=!FILEPATH:*%cd%\=!

rem set all the corresponding environment variables
call %REL_FILEPATH%auto_version.bat %*

set OUTPUT_FILE=auto_version.fi
echo       CHARACTER*6 VS_VERSION /'%AVEXPORT_VS_VERSION%'/ > %OUTPUT_FILE%
echo       CHARACTER*100 GIT_BRANCH /'%AVEXPORT_GIT_BRANCH%' / >> %OUTPUT_FILE%
echo       CHARACTER*100 GIT_VERSION /'%AVEXPORT_GIT_VERSION%'/ >> %OUTPUT_FILE%
echo       CHARACTER*100 GIT_VERSION_SHORT /'%AVEXPORT_GIT_VERSION_SHORT%'/ >> %OUTPUT_FILE%
echo       CHARACTER*10 COMPILE_DATE /'%AVEXPORT_DATE%'/ >> %OUTPUT_FILE%
echo       CHARACTER*10 PLATFORM /'%AVEXPORT_PLATFORMNAME%'/ >> %OUTPUT_FILE%
echo       CHARACTER*10 CONFIGURATION /'%AVEXPORT_CONFIGURATIONNAME%'/ >> %OUTPUT_FILE%
echo       CHARACTER*10 VERSION_NUMBER /'%AVEXPORT_TAG_DATE%'/ >> %OUTPUT_FILE%

rem touch the prog_version.f90 file if it exists in the project directory
rem   or else it could use stale version names for windows
set PROG_VERSION_SOURCE=git_vc_autoversion\prog_version.f90
if exist %PROG_VERSION_SOURCE% (
  type nul >> %PROG_VERSION_SOURCE%
)

endlocal