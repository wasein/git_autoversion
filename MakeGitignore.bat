set pattern="ignore*.txt" 

if exist %pattern% (
	>hookoutput.log 2>&1 (
		del .gitignore
		for %%f in (%pattern%) do type %%f >> .gitignore 
		copy %0 .
	)	
)

