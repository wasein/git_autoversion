#!/bin/bash

# not really necessary but can make troubleshooting less verbose
REL_FILEPATH=$(dirname $(realpath --relative-to=$PWD $0) )

# set all the corresponding environment variables
. ${REL_FILEPATH}/auto_version.bash $*

OUTPUT_FILE=auto_version.h
INCLUDE_GUARD=__AUTOVERSION_H

cat > $OUTPUT_FILE <<UNIQUE_MESSAGE
#ifndef $INCLUDE_GUARD
#define $INCLUDE_GUARD

extern const char vs_version[];
extern const char git_branch[];
extern const char git_version[];
extern const char git_version_short[];
extern const char compile_date[];
extern const char platform[];
extern const char configuration[];
extern const char version_number[];

#endif //$INCLUDE_GUARD
UNIQUE_MESSAGE

OUTPUT_FILE=auto_version.c
cat > $OUTPUT_FILE <<UNIQUE_MESSAGE 

const char vs_version[] = "$AV_VS_VERSION";
const char git_branch[] = "$AV_GIT_BRANCH";
const char git_version[] = "$AV_GIT_VERSION";
const char git_version_short[] = "$AV_GIT_VERSION_SHORT";
const char compile_date[] = "$AV_DATE";
const char platform[] = "$AV_PLATFORMNAME";
const char configuration[] = "$AV_CONFIGURATIONNAME";
const char version_number[] = "$AV_TAG_DATE";

UNIQUE_MESSAGE

