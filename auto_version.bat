@echo off
setlocal enabledelayedexpansion

set AV_CONFIGURATIONNAME=%1
if [!AV_CONFIGURATIONNAME!]==[] (
  set AV_CONFIGURATIONNAME=undefined
)
set AV_PLATFORMNAME=%2
if [!AV_PLATFORMNAME!]==[] (
  set AV_PLATFORMNAME=undefined
)

set VSINSTALLDIR=%3

rem check if git exists
git --version >nul 2>&1

if ERRORLEVEL 1 (
  set GIT=0
  set AV_GIT_VERSION=no-git
  set AV_GIT_VERSION_SHORT=no-git
) else (  
  rem check if this is actually a git repository
  git describe --always >nul 2>&1 
  
  if ERRORLEVEL 1 (
    set GIT=0
    set AV_GIT_VERSION=non-git
    set AV_GIT_VERSION_SHORT=non-git
  ) else (
    set GIT=1
    for /F "tokens=*" %%a in ('git describe --tags --always') do set AV_GIT_VERSION=%%a
    for /F "tokens=*" %%b in ('git describe --tags --always --abbrev^=0') do set AV_GIT_VERSION_SHORT=%%b
    for /F "tokens=*" %%c in ('git --no-pager log -1 --format^=%%ai !AV_GIT_VERSION_SHORT!') do set GIT_TAG_UTIME=%%c
    for /F "tokens=*" %%d in ('git rev-parse --abbrev-ref HEAD') do set AV_GIT_BRANCH=%%d
    git diff --quiet
    if ERRORLEVEL 1 (
      set AV_GIT_VERSION=!AV_GIT_VERSION!-working
    )
  )
)

rem figure out the Visual Studio version
rem  removing trailing slash passed from visual studio
if [!VSINSTALLDIR!]==[] (
  set AV_VS_VERSION=undefined
) else (  
  rem remove quotes
  set VSINSTALLDIR=!VSINSTALLDIR:"=!
  rem use the last word as the version number
  for %%i in (!VSINSTALLDIR!) do set AV_VS_VERSION=%%i
  rem remove residual trailing backslash  
  set AV_VS_VERSION=!AV_VS_VERSION:\=!
)

if %GIT%== 1 (
  set AV_TAG_DATE=%GIT_TAG_UTIME:~5,2%%GIT_TAG_UTIME:~8,2%%GIT_TAG_UTIME:~0,4%
) else (	
  rem use arbitrary old date
  set AV_TAG_DATE=01182017
)

set AV_DATE=%DATE:~4,10%

endlocal & (
  rem UGLY but it will have to do for consistency
  set AVEXPORT_VS_VERSION=%AV_VS_VERSION%
  set AVEXPORT_GIT_BRANCH=%AV_GIT_BRANCH:~2%
  set AVEXPORT_GIT_VERSION=%AV_GIT_VERSION%
  set AVEXPORT_GIT_VERSION_SHORT=%AV_GIT_VERSION_SHORT%
  set AVEXPORT_DATE=%AV_DATE%
  set AVEXPORT_PLATFORMNAME=%AV_PLATFORMNAME%
  set AVEXPORT_CONFIGURATIONNAME=%AV_CONFIGURATIONNAME%
  set AVEXPORT_TAG_DATE=%AV_TAG_DATE%
)