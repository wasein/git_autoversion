import git

class AutoVersion:
    def __init__(self, working_dir='.'):
        gitcmd = git.cmd.Git(working_dir)
        
        self.version = gitcmd.describe(always=True, tags=True)
        self.version_short = gitcmd.describe(always=True, tags=True, abbrev=0)
        self.branch = gitcmd.rev_parse('HEAD', abbrev_ref=True)
        self.date = gitcmd.log('HEAD', n=1, format='%ad', date='short')
                
        try:
            gitcmd.diff(quiet=True)
        except git.exc.GitCommandError:
            self.version += '-working'
            
