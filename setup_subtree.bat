rem run in new git repo directory with full net path
rem   e.g. \\fsgp\home\milldt\src\git_vc_autoversion\setup_subtree.bat
@echo off

setlocal enabledelayedexpansion

set AUTOVERSION_PATH=%~dp0
rem remove trailing slash
set AUTOVERSION_PATH=%AUTOVERSION_PATH:~0,-1%
for %%i in (%AUTOVERSION_PATH%) do set SUBTREE_NAME=%%~ni

rem switch to slashes
set AUTOVERSION_PATH=%AUTOVERSION_PATH:\=/%

git remote add %SUBTREE_NAME% %AUTOVERSION_PATH%
git subtree add --prefix=%SUBTREE_NAME% %SUBTREE_NAME% master --squash

