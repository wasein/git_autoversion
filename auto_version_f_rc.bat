@echo off

setlocal enabledelayedexpansion

rem not really necessary, but can make troubleshooting less verbose
set FILEPATH=%~dp0
set REL_FILEPATH=!FILEPATH:*%cd%\=!

call %REL_FILEPATH%auto_version_f.bat %*
call %REL_FILEPATH%auto_version_rc.bat %*

exit /b 0