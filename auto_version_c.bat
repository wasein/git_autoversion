@echo off

setlocal enabledelayedexpansion

rem not really necessary but can make troubleshooting less verbose
set FILEPATH=%~dp0
set REL_FILEPATH=!FILEPATH:*%cd%\=!

rem set all the corresponding environment variables
call %REL_FILEPATH%auto_version.bat %*

set OUTPUT_FILE=auto_version.h
set INCLUDE_GUARD=__AUTOVERSION_H
echo #ifndef %INCLUDE_GUARD% > %OUTPUT_FILE%
echo #define %INCLUDE_GUARD% >> %OUTPUT_FILE%
echo namespace autoversion { >> %OUTPUT_FILE%
echo   extern const char vs_version[]; >> %OUTPUT_FILE%
echo   extern const char git_branch[]; >> %OUTPUT_FILE%
echo   extern const char git_version[]; >> %OUTPUT_FILE%
echo   extern const char git_version_short[]; >> %OUTPUT_FILE%
echo   extern const char compile_date[]; >> %OUTPUT_FILE%
echo   extern const char platform[]; >> %OUTPUT_FILE%
echo   extern const char configuration[]; >> %OUTPUT_FILE%
echo   extern const char version_number[]; >> %OUTPUT_FILE%
echo }; >> %OUTPUT_FILE%
echo #endif // %INCLUDE_GUARD% >> %OUTPUT_FILE%

set OUTPUT_FILE=auto_version.cpp
echo namespace autoversion { > %OUTPUT_FILE%
echo   extern const char vs_version[] = "%AVEXPORT_VS_VERSION%"; >> %OUTPUT_FILE%
echo   extern const char git_branch[] = "%AVEXPORT_GIT_BRANCH%"; >> %OUTPUT_FILE%
echo   extern const char git_version[] = "%AVEXPORT_GIT_VERSION%"; >> %OUTPUT_FILE% 
echo   extern const char git_version_short[] = "%AVEXPORT_GIT_VERSION_SHORT%"; >> %OUTPUT_FILE%
echo   extern const char compile_date[] = "%AVEXPORT_DATE%"; >> %OUTPUT_FILE%
echo   extern const char platform[] = "%AVEXPORT_PLATFORMNAME%"; >> %OUTPUT_FILE%
echo   extern const char configuration[] = "%AVEXPORT_CONFIGURATIONNAME%"; >> %OUTPUT_FILE%
echo   extern const char version_number[] = "%AVEXPORT_TAG_DATE%"; >> %OUTPUT_FILE%
echo }; >> %OUTPUT_FILE%

endlocal
