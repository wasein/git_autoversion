#!/usr/bin/env tclsh

proc isGit {} {
    if [catch {exec git --version} result] {
        # no Git installed so we can not tell
        return 0
    }
    
    if [catch {exec git describe --always} result] {
        # no Git version for the directory
        return 0
    }
    return 1
}

proc globReplaceTime {inFilename outFilename} {
    set systemTime [clock seconds]

    # date
    set compileYear [clock format $systemTime -format %y]
    set compileMonth [clock format $systemTime -format %m]
    set compileDay [clock format $systemTime -format %d]

    # time
    set compileHour [clock format $systemTime -format %H]
    set compileMinute [clock format $systemTime -format %M]

    # open the batch compile time file
    set compileBatchFile [open $inFilename]
    set contents [read $compileBatchFile]
    close $compileBatchFile

    # do the substitutions on the contents
    regsub -all %compile_year_batch% $contents $compileYear contents
    regsub -all %compile_month_batch% $contents $compileMonth contents
    regsub -all %compile_day_batch% $contents $compileDay contents
    regsub -all %compile_hour_batch% $contents $compileHour contents
    regsub -all %compile_minute_batch% $contents $compileMinute contents

    # output to the build time file    
    set compileBatchFileOut [open $outFilename w]
    puts $compileBatchFileOut $contents
    close $compileBatchFileOut 
    
    return $systemTime
}

proc globReplaceGitVersion {inFilename outFilename} {
    set gitAscii [exec git describe --tags --always]
    set gitList [split $gitAscii .-] 
    set versionMajor [lindex $gitList 0]
    set versionMinor [lindex $gitList 1]
    set bugFix [lindex $gitList 2]
    if {[llength $gitList] > 3} {
        set commits [lindex $gitList 3]
    } else {
        # no commits in tag description
        set commits 0
    }
    # exclude "xpr" Vivado files since they tend to have obnoxious changes that we don't want to consider "working"
    set gitWorking [catch {exec git diff --quiet -- ":(exclude)*.xpr"} result]

    set gitBatchFile [open $inFilename]
    set contents [read $gitBatchFile]
    close $gitBatchFile

    # do the substitutions on the contents
    regsub -all %git_ascii% $contents $gitAscii contents
    regsub -all %version_major% $contents $versionMajor contents
    regsub -all %version_minor% $contents $versionMinor contents
    regsub -all %bugfix% $contents $bugFix contents
    regsub -all %commits% $contents $commits contents
    regsub -all %working% $contents $gitWorking contents

    # output to the git version file    
    set gitBatchFileOut [open $outFilename w]
    puts $gitBatchFileOut $contents
    close $gitBatchFileOut 
}
