@echo off

setlocal enabledelayedexpansion

rem not really necessary but can make troubleshooting less verbose
set FILEPATH=%~dp0
set REL_FILEPATH=!FILEPATH:*%cd%\=!

rem set all the corresponding environment variables
call %REL_FILEPATH%auto_version.bat %*

set OUTPUT_FILE=%FILEPATH%\auto_version.txt
echo %AVEXPORT_GIT_VERSION% > %OUTPUT_FILE% 
