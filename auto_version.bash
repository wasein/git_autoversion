#!/bin/bash

if [[ $# > 1 ]]; then
	AV_CONFIGURATIONNAME=$1
else
	AV_CONFIGURATIONNAME=undefined
fi

if [[ $# > 2 ]]; then
	AV_PLATFORMNAME=$2
else
	AV_PLATFORMNAME=$(uname -o -m)
fi

# check if git exists
git --version >/dev/null 2>&1

if [[ $? -ne 0 ]]; then
  GIT=0
  AV_GIT_VERSION=no-git
  AV_GIT_VERSION_SHORT=no-git
else 
  # check if this is actually a git repository
  git describe --always >/dev/null 2>&1 
  
  if [[ $? -ne 0 ]]; then
    GIT=0
    AV_GIT_VERSION=non-git
    AV_GIT_VERSION_SHORT=non-git
  else
    GIT=1
	AV_GIT_VERSION=$(git describe --tags --always)
	AV_GIT_VERSION_SHORT=$(git describe --tags --always --abbrev=0)    
	GIT_TAG_UTIME=$(git --no-pager log -1 --format=%ai ${AV_GIT_VERSION_SHORT})
    AV_GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    git diff --quiet
    if [[ $? -ne 0 ]]; then
      AV_GIT_VERSION=${AV_GIT_VERSION}-working
    fi
  fi
fi

# figure out the gcc version (Use the VS variable for now)
AV_VS_VERSION=$(gcc -dumpversion)

if [[ ${GIT} -eq 1 ]]; then
  AV_TAG_DATE=${GIT_TAG_UTIME:5:2}${GIT_TAG_UTIME:8:2}${GIT_TAG_UTIME:0:4}
else
  # use arbitrary old date
  AV_TAG_DATE=01182017
fi

AV_DATE=$(date +%D)

