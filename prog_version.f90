module prog_version
  private
  
  character(len=50) progname
  character(len=100) progversion
  character(len=8) version_date
  character(len=120) prog_dir
  
  character(len=1) qm, delim ! a bit bizzare in use
  
  public progname, progversion, version_date, prog_dir
  public qm, delim
  public setautodlgtitle, setautoqwintitle, SetVersionDate, WriteHeaderVersion, SetProgDir
  
  contains
  
  subroutine setautodlgtitle(desc, dlg)        
    use ifqwin
    use iflogm ! for dialog type   
        
    include 'auto_version.fi'
    
    character(len=*) desc
    TYPE (DIALOG) DLG
    
    character(len=159) title 
    ! 50 for progname, 6 for '-devel', 100 for git_version, 3 for space
		
	if (git_branch .eq. 'devel') then
	    title = trim(progname) // '-devel ' // trim(git_version)
    else
        title = trim(progname) // ' ' // trim(git_version_short)
    endif
    
    if (desc .eq. '') then
        call dlgsettitle(dlg, title // ''c )
    else    
        call dlgsettitle(dlg, trim(desc) // ' (' // title // ')' // ''c )
    endif
  end subroutine 
   
  subroutine setautoqwintitle        
    use ifqwin
    use ifwin ! need to set title
    
    include 'auto_version.fi'
    
    logical status
    character(len=159) title 
    ! 50 for progname, 6 for '-devel', 100 for git_version, 3 for space
		
	if (git_branch .eq. 'devel') then
	    title = trim(progname) // '-devel ' // trim(git_version)
    else
        title = trim(progname) // ' ' // trim(git_version_short)
    endif
    
    status = setwindowtext(gethwndqq(QWIN$FRAMEWINDOW), title // ''c)	
  end subroutine 
    
  subroutine SetVersionDate          
    implicit none  
    include 'auto_version.fi'
    integer*4 month, day, year
   
    read(version_number,'(I2,I2,I4)') month, day, year
    write(version_date,"(I2,'/',I0,'/',I2.2)") month,day,year-2000
  end subroutine
    
  subroutine WriteHeaderVersion(unit, string2)    
    implicit none
    include 'auto_version.fi'
    
    integer, parameter :: linewidth = 132
    character(len=*) string2    
	character(len=linewidth) string
	integer*4 unit
	integer w1,w2,w3,w4
			
	if (git_branch .eq. 'devel') then 
	    string = ' ' // trim(progname) // ' ' // trim(version_date) // ' ' // trim(git_version)
	else
	    string = ' ' // trim(progname) // ' ' // trim(version_date) // ' ' // trim(git_version_short) 
	endif
			
    w1 = len_trim(string2) + 1 ! include trailing space
	w2 = 10 ! mid stars
	w3 = len_trim(string) + 1
	w4 = max(1, linewidth - 5 - w1 - 1 - w2 - 1 - w3 - 1)

    write(unit,9500) string2(:w1), string(:w3)      
    9500  FORMAT('**** ',A,X,<W2>('*'),X,A,<W4>('*'))
  end subroutine

  subroutine SetProgDir
    implicit none
    
    integer i,n
    
    call get_command(prog_dir)
	i = scan(prog_dir,'/\',back=.true.) + 1
	delim = prog_dir(i-1:i-1)
	PROGNAME = trim(PROGNAME)//' <'//trim(prog_dir(i:))//'>'
	qm = ' '
	n = scan(prog_dir,"""'`")
	if (n == 1) then
		n = len_trim(prog_dir)
		if (prog_dir(:1)==prog_dir(n:n)) then
			qm = prog_dir(:1)
		end if
	end if
	prog_dir(i:) = ''	! leave only the drive and directory
	if (qm /= ' ') then
		n = len_trim(PROGNAME)
		PROGNAME(n-1:n) = PROGNAME(n:n)
	end if
  end subroutine	
	
end module 